﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProyect
{
    public class Cuota
    {
        public int  Id_Cuota { get; set; }
        public double Valor_Cuota { get; set; }
        public double Capital_Cuota { get; set; }
        public double Interes_Cuota { get; set; }
        public double Balance_Cuota { get; set; }

        public Cuota(int id,double valor, double capital, double interes, double balance) 
        {
            this.Id_Cuota = id;
            this.Valor_Cuota = valor;
            this.Capital_Cuota = capital;
            this.Interes_Cuota = interes;
            this.Balance_Cuota = balance;
        }
    }
}
