﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic; // Importamos la clase de .Net que nos ayuda sacar la cuota

namespace FinalProyect.CalculadoraDePrestamos
{
    public class Prestamo
    {
        public double montoIngresado { get; set; }
        public double tasaAnual { get; set; }
        public DateTime FechaPrestamo { get; set; }
        public double tasaMensual { get; set; }
        public double plazoPrestamo { get; set; }
        public double Cuota { get; set; }
        public double Balance { get; set; }
        public List<Cuota> ListaDeCuotas;
        

        public Prestamo(double montoIngresado, double tasaAnual, double plazoPrestamo) 
        {
            this.montoIngresado = montoIngresado;
            this.tasaAnual = tasaAnual;
            this.tasaMensual = (tasaAnual / 12) / 100; // sacamos la tasa mensual
            this.plazoPrestamo = plazoPrestamo;
            this.Cuota = -Financial.Pmt(tasaMensual, plazoPrestamo, montoIngresado);  // sacamos la cuota con la clase importada
            this.Balance = montoIngresado;
            this.ListaDeCuotas = new List<Cuota>();
            this.FechaPrestamo = DateTime.Now;
        }

        public void AgregarALista(Cuota nuevaCuota) 
        {
            ListaDeCuotas.Add(nuevaCuota);
        }
    }
}
