﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic; // Libreria de .NET que ayuda a sacar la cuota 

namespace FinalProyect.CalculadoraDePrestamos
{
    class Program // Luis Rafael Coronado - 2020-10295
    {
        static void Main(string[] args)
        {
            // Asignamos el monto a prestar
            Console.Write("\nIngresa el Monto: ");
            double montoIngresado = Convert.ToDouble(Console.ReadLine());

            //Asignamos las tasa Anual 
            Console.Write("\nIngresa la Tasa Anual: ");
            double tasaAnual = Convert.ToDouble(Console.ReadLine());

            // Obtenemos el tiempo en meses 
            Console.Write("\nIngresa el plazo (en meses): ");
            double plazoPrestamo = Convert.ToDouble(Console.ReadLine());

            // creamos el prestamo
            Prestamo nuevoPrestamo = new Prestamo(montoIngresado, tasaAnual, plazoPrestamo);


            Console.WriteLine($"\nMonto: {montoIngresado}");
            Console.WriteLine($"Tasa: {tasaAnual}");
            Console.WriteLine($"Plazo: {plazoPrestamo} meses");

            Console.WriteLine(Environment.NewLine + "|No.|\tCuota|\tCapital|\tInteres|\tBalance|" + Environment.NewLine);
            double Balance = nuevoPrestamo.Balance;

            for (int i = 1; i <= plazoPrestamo; i++) // iteramos a traves de los meses
            {
                // Creamos una clase cuota
                Cuota nuevaCuota = new Cuota(i, nuevoPrestamo.Cuota, nuevoPrestamo.Cuota - (Balance * nuevoPrestamo.tasaMensual), Balance * nuevoPrestamo.tasaMensual, Balance);

                // agregamos la cuota a la lista de cuotas
                nuevoPrestamo.AgregarALista(nuevaCuota);

                // restamos el balance
                Balance -= nuevaCuota.Capital_Cuota;

                Console.WriteLine($"|{i}|\t{nuevaCuota.Valor_Cuota:C}|\t{nuevaCuota.Capital_Cuota:C}|\t{nuevaCuota.Interes_Cuota:C}|\t{Balance:C}|" + Environment.NewLine);
            }

            Console.WriteLine("Se completo correctamente el simulador.");
            Console.ReadLine();
        }
    }
}
